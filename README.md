## de-exagger

#### Identify exaggerated articles, and expose them out. Potentially go so far you can de-exaggerate an article. 


#### What

So here's the deal. 
You come across a lot of articles, novels, books, and essentially any kind of text. (Even trascripts that are generated from audios!)
We're floating in the sea of language, and the sea is polluted with unnecessary exaggeration. 


#### Why

What if, just what if we're able to identify that exaggeration from the article(text)? And potentially filter them out so we end up with just simple raw content. 
Just raw, digestible, comprehensible content that is devoid of bias as much as possible? 

And that's what de-exagger is hoping to solve. 

#### How? 

Well, that's what the code is supposed to do.

##### Version 0.1
* Given a text, you'll be able to let us know if that text/article is exaggerated. (Can be a simple python script)
* You define the output! 
    * However, ensure that it is meaningful and *why*
    * Just make sure it solves the problem

##### Version 0.2
* Inputs can be in multiple formats. 
    * Text
    * Text file
    * URL of an article

* Output is of the format:
    - Level should be 1-10, 1 being the lowest and 10 being the highest. 
        ```
        {
            "exaggeration": {
                "level": "10",
                "exaggeratedWords": [
                    "SomeWord",
                    "SomeOtherWord",
                    "UnnecessaryWord"
                    ]
        }


##### Version 0.5

A command line application of the above. 

##### Version 1.0

A python library of the same. 


### Coding guidelines. 

* You're not restricted to use any and all tools that is required to get the work done. However, ensure it doesn't involve any properietry external APIs. 
* It's a must that code should be functional, clean, commented, and neat. In that order. 
* Code comments should be as clear as possible. Code without comments are not welcome. But don't comment just for the sake of commenting. 
* Always push to the develop branch. (You can't push to the master branch!)
    * Once pushed to the develop branch, raise a pull request for me to review. 
* Commit with proper committing guidelines. You can follow anything for now. 


##### IMPORTANT NOTE

You're not expected to do all the versions. But doing many of the versions enables us to gauge your skills better. 🙂
